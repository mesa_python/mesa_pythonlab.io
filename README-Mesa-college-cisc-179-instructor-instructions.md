## Editing content

- edit `src/content/blog/*.md`
- images go in `static/images`
- python code snippets go in `static/python`
- SVG can be exported from drawio and embedded in web pages


## Instructor TODO each semester

1. fork cisc-179 repo and name cisc-179-previousyear-previousseason
2. make sure all mesa_python.gitlab.io links reference mesa_python/cisc-179/ rather than e.g. .com/hobs/cisc-179-2024-spring/
3. copy course into development shell?