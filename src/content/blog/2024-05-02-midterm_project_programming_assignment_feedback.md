---
title: 'Midterm project feedback'
description: 'Feedback and grades on your midterm project.'
pubDate: 'May 2, 2024'
---

You should see some feedback on your midterm project in your e-mail inbox.

If your project utilized at least 4 conditionals and 4 different "rooms" you should get 100%.
I may be able to provide some tips to help you improve it for the Final Project due at the end of the course.