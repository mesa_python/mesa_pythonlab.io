---
title: '5.3 Assisted Development (Program Synthesis and the Future of Software Development)'
description: "Mesa College Intro to Python -- Things to watch out for in your assisted development."
pubDate: 'Feb 27, 2025'
slides:
    theme: simple
    # black (default)
    # beige
    # white   White background, black text, blue links
    # league  Gray background, white text, blue links
    # beige   Beige background, dark text, brown links
    # night   Black background, thick white text, orange links
    # serif   Cappuccino background, gray text, brown links
    # simple  White background, black text, blue links
    # solarized   Cream-colored background, dark green text, blue links
    # moon    Dark blue background, thick grey text, blue links
    # dracula
    # sky Blue background, thin dark text, blue links
    # blood
    highlight_theme: vs
    # separator_vertical: ^\s*--v--\s*$

    separator: ^\s*---\s*$  # <!--s-->
revealjs:
    height: 1080
    width: 1920
    slideNumber: "h.v"
---

# Assisted Development  [<sup>^0</sup>](#links)

#### Program synthesis and the future of software development

by Hobson Lane (mesa_python.gitlab.io)

shoutout to Maria Dyshel, Vishvesh Bhat

---

## Agenda

1. Assisted development
2. Neurosymbolic reasoning
3. Program synthesis example
4. Shy fun

---

## Assisted development

1. Copilot+VSCode|JetBrains|Neovim
1. Cursor.sh+VSCode|NeoVim|Jetbrains - $20/mo - just bought Supermaven CLI
1. Windsurf+Codium (VSCode FOSS fork) - "agentic IDE"
1. Zed - FOSS Rust by maker of Github's Atom - focus on fast and eventually AI - 3 yr old almost feature parity with Sublime and VSCode
1. Aider+terminal
1. Cline+VSCode|Jetbrains
1. shy-sh - FOSS Python CLI developer assisstant
1. Gemini, Phind, Perplexity (Type 2 Assisted Development)

---

## Program synthesis

1. **Deduction** (or Compiling) - High level languages (Logic, Haskel, Python)
2. **DSL** - including declarative languages (f-strings, Jinja2, Ansible/puppet/Terraform yaml)
3. **Induction** (inference) - ML set of input output examples, verifier, or reverse engineering an existing system, like George hotz tiny corp jailbreaking an iphone

---

## Examples of deduction

1. Python 2to3
2. regular expressions: ``r'(for|while|if|else|with|try|except|def)(.*):``
3. Juan's HDMI screen capture OCR Python->Rust

---

## Next token prediction

<a href="/images/stochastic-chameleon-beam-search.drawio.png"><img src="/images/stochastic-chameleon-beam-search.drawio.png" width="1024" height="768" /></a>

---

## Assisted Development [<sup>^1</sup>](#1) [<sup>^2</sup>](#1)

1. Autocomplete (Tab-Nine, Sublime, Augment Code)
2. Search (Phind.com, Perplexity.ai)
3. Conversation (ChatGPT, shy-sh)

4. Neurosymbolic reasoning - Shoulder-surfing AI (Anthropic "computer use")

---

## Execute a command in Bash?

```bash
cmd="ls -l"
eval " $cmd"
```

---

## David Crenshaw

"Only 5% of developers use conversational AI to code" <sup>[^3](#links)</sup> <sup>[^4](#links)</sup>

(Type 3 Assisted Development)

---

## Assisted Misalignment

Your favorite LLM was poisoned (fine-tuned) on misaligned and insecure code.[^1]

---

#### LLM:

```python
eval $(echo $command_from_some_guy_on_the_internet)
```

---

#### StackOverflow:

---

### Links

TBD proai.org shortURLs and mesa_python.gitlab.io blog post

- [^0]: [proai.org/assisted-development](https://mesa_python.gitlab.io)
- [^1]: [proai.org/assisted-misalignment](https://techhub.social/users/Techmeme/statuses/114074167504717661) - Mastodon Toot by @Techmeme
- [^2]: [proai.org/assisted-misalignment-ars](https://arstechnica.com/information-technology/2025/02/researchers-puzzled-by-ai-that-admires-nazis-after-training-on-insecure-code/
http://www.techmeme.com/250227/p2#a250227p2) - Article by Ars Technica
- [^3]: [proai.org/3-types](https://crawshaw.io/blog/programming-with-llms) - How I Program with LLMs by David Crenshaw (CEO Tailscale)
- [^4]: [changelog.com/podcast/629](https://changelog.com/podcast/629) - Jerod interviews David Crenshaw (Tailscale)

----

## neurosymbolic reasoning
examples from ARC challenge
dsl graph search diagram (exponential complexity)
if search on tokens with beam search it's just depth-first stochastic search of the wrong graph
neurosymbolic reasoning searches the graph of valid programs - like AlphaFold, AlphaGo, or AlphaCode

- [ ] neurosymbolic reasoning
 - [-] optional slide comparing computer use, puppet and Ansible agents vs "operator" 200/mo https://git.lolcat.ca/lolcat/4get
- [ ] slide listing 3 uses of LLMs and shout out to tailscale CEO blog post
- [ ] 1. slide on code completion in sublime
- [ ] 2. slide on phind and perplexity and compare to metager and https://git.lolcat.ca/lolcat/4get
- [ ] 3. slide on cognitive load of translating concept to conversation
- [ ] snake game reinforcement learning demo example with Claude
- [ ] Gemini vs claude vs augment code

- [ ] slide on zed


---

- [ ] slide on Regex to match python keywords and a compiler optimization vs ml ngram approach


---

## Backup (notes)

1. Deduction or Compilers or transpilers Fortran Haskell python decorators & factories high level languages, Wikipedia max example
2. Declarative & domain specific languages and fstrings  Jinja2 Django templates and terraform Ansible puppet yaml declarative languages generating functional programs
3. Induction or inference - ML set of input output examples, verifier, or reverse engineering an existing system, like George hotz tiny corp jailbreaking an iphone
<br>
<br>
4. Neurosymbolic reasoning agents  - Shoulder-surfing AI ("computer use")

---

## code synthesis slides
1. Math function synthesis from examples or running of a verifier (each example is expensive to verify, using an optimizer or simulator).
1. Simulation verifier would work for game play or puzzle/maze solving program generation. Generating a program that can solve any maze, input=maze, output=path
1. Program synthesis, [automatic] code generation, compiler, templating engine, high level [templating] programming language, UML, low code platforms
2. Deriving a regex from example matches
3. Deriving a dialog engine or chatbot from example conversations
3. Math function example max() from logical specification, build on it to create sort pair from max, then max of list, then merge 1 list with 1 val (binary search), then merger sort verifier for both. Functional programming
4. Machine learning, generalization, regularization, ocams razor,
5. A star, depth first search, breadth first search, beam search, but need continuous heuristic or metric to say how close you are to optimum program. LLMs really good at generating almost correct programs, so great for depth first search.
6. If you can break problem into smaller function you can do intermediate depth search for exponential speedup
7. Current research is focused on improving the accuracy rate on complete programs which will never succeed because Portugal space is far larger than dataset, not spanned by possible programs
8. Neural network architectures and weights (dl training) are the ultimate in program synthesis, which could be very modular by focusing on layers that are similar
9. Genetic algorithms for program synthesis
10. Toy problem regex to recognize a valid pep8 compliant Python...
    - keyword
    - int
    - float
    - variable/function name
    - raw string literal
    - string literal
    - dict, tuple, list, argument, type hints, function signature
    - for loop, list comprehension, generator
11. A regex is a grammar and a grammar can be generated/enumerated.
12. A big refinement would be to constrain the llm token generator, with the Python grammar and vocabulary so that it can never generate a syntax error or non-pep8 code.
13. Other heuristics could be encoded in the grammar like cyclomatic complexity variable name vocabulary and length limitations and abbreviation control and Hungarian notation, or NL grammar rules (active verbs for functions, is for bool retvals, _to_ for transformation, no pattern not already available in standard Python library of builtins, no list comprehensive, just accumulators, all functions with type hints and args on one line.
