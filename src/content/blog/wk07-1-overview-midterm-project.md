---
title: '7.1 Overview'
description: 'The things you need to get started building a text adventure game.'
pubDate: 'Mar 10, 2024'
---

# 7.1 Overview

It's go time!
You finally get to use your Python skills create a complete Python program, from start to finish. This week you will:

1. Make sure you can create a `*.py` file
2. Write a story for your text adventure game
3. Sketch a diagram of your Python logic tree of conditionals (``if``s)
4. Write the Python code
5. Iteratively test and edit your Python code
6. Upload your game to GitLab
7. Enjoy watching others play your game

## Prerequisites

Do you have the tools you will need for this assignment.
If you are missing any of these 4 tools, click on the appropriate link to learn how to install and run it:

#### 1. An interactive Python console:
- **[Anaconda qtPython](https://docs.anaconda.com/free/anaconda/install/windows/) (all OSes)**
- Jupyter console (in Anaconda)  
- iPython or Python (built into Mac & Linux)
#### 2. A text editor:
- **Spyder (built into Anaconda)**
- [PyCharm](https://www.jetbrains.com/pycharm/download/)
- [Sublime Text](https://www.sublimetext.com) (for advanced students)
- [Zed](https://zed.dev/) (Mac only)
#### 3. A command line shell to organize files on your laptop: 
  - **[git-bash](https://gitforwindows.org/) (for Windows)**
  - `sh`, `bash`, `zsh`, (built into Linux and Mac)
#### 4. Interactive textbook - _Foundations of Python Programming_
  - **Runestone.Academy course named ["mesa_python"](https://runestone.academy/ns/books/published/mesa_python)**.

If you aren't sure how to use all these tools, watch the following videos.
This first video will help you install and use Anaconda:

<iframe width="560" height="315" src="https://www.youtube.com/embed/vwtvhFyqu3U?si=drD_gmaaJdlpZl-n" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

You will need to know how to create and run Python programs in order to complete this assignment.
One of the steps that is confusing for some students, is finding and running a Python file on your computer.
Here's a video that can help you use the interactive Python console to find and import or run a Python file (`*.py`).
Working with file paths in Windows is especially tricky.
Here's a video that shows you how to use `bash` commands within a Python console on Windows.
Bash is a Linux command line program for manipulating files on your computer.

Here is a video by @chinamatt explaining how to use Bash on Windows: [Install and Use Python in Git Bash for Windows 10](https://www.youtube.com/watch?v=M33oOq-c60s)

A Python program is a plain text file with the extension `.py` (sometimes written `*.py`).
The ".py" at the end lets your operating system know what application to run when it needs to read and run that file.
For example you might want to call your text adventure Python program `text_adventure.py`.
Python files must be valid Python variable names (except for the extension).
You found this out in Chapter 3 of Foundations of Python Programming when you learned how to `import` modules.
A module is just a Python text file.
So your program file name should only contain alphanumerc lowercase letters (``a``-``z``), uppercase letters (``A``-``Z``), digits (``0``-``9``) and underscores (``_``).
Do not use dashes (hyphens: ``-``) or spaces (`` ``) in your Python
program file name.

## What is a text adventure?

Have you ever read a _choose your own adventure_ (CYOA) novel?
A CYOA book lets the reader decide what the hero does at critical points in in the book.
The author writes all the different story lines and tells you which page to turn to when you pick a particular action?

What about role playing games?
Have you ever played a text adventure video game or role playing game with your friends?
In role playing, one of your friends is the _Dungeon Master_ (DM). 
The DM describes a world to you and your "party" of adventurers, and you all decide what to do at each turn in the game.
In a text adventure, a computer program acts as the Dungeon Master.
And the computer describes the world with text rather than with voice (and hand waving ;).

Don't worry, you do not have to create a Dungeon Master program for an entire DnD campaign!
You will only need to plan out a "dungeon" with 4 rooms or turns.
And I will give you the Python program that implements a game with 3 rooms. You just need to edit the Python code to replace the description strings for my rooms with yours.
And then you will add a few more Python expressions to add another room.

If you can demonstrate the concepts you learned in Runestone over the past few weeks and build a working Python program, you will receive an A on this midterm project.
Do you remember how to use each of these Python expressions?
These are the things you need to show me that you understand:

- `print` -- display text
- `input` -- retrieve user input text
- `=` -- variable assignment
- `for` and `in` -- loops and iteration
- `if`, `elif` and `else` -- conditional logic
- `==`, `>`, `<`, `>=`, `<=` -- boolean conditions
- `[-1]`, `[0]` -- indexing an array using integers
- `['countdown', 3, 2.0, 1, 'launch', '!']` -- create lists of objects
- `[:3]` and `range(3)` -- slicing and generating sequences
- `import random` -- importing built-in Python modules

