---
title: '3.3 Use Linux Inside Sublime'
description: "Mesa College Intro to Python -- Linux inside Sublime."
pubDate: 'Feb 16, 2025'
---

# Linux inside Sublime
Open Sublime Text so you can start working on your Python program.

## Save your project settings
1. In Sublime, go to the Project menu and choose "Save Project As..."
[![sublime project menu save as ...](/images/project-save-project-as.png)](/images/project-save-project-as.png)
[![project-save-project-as](/images/project-menu-open-save-project-as-highlighted.png)](/images/project-menu-open-save-project-as-highlighted.png)
2. Find your code folder that you created last week and change your project settings file from ``untitled.sublime-project`` to ``code.sublime-project``
[![project-save-project-as-rename](/images/project-save-project-as-rename-untitled-to-code.png)](/images/project-save-project-as-rename-untitled-to-code.png)
3. Click [Save] button to create a ``code.sublime-project`` file in your code folder


## Install Terminus package in Sublime Text
Terminus is a plugin that allows you to interact with your computer using a Terminal (text interface).
You need this to be able to program your computer and move your Python program files around easily.
1. In Sublime hit [ctrl-shift-P] **OR** select "Command Pallette" in the "Tools" menu
[![screenshot_install_package](/images/ctrl-shif-p-install-package.png)](/images/ctrl-shif-p-install-package.png)
2. type "install" and hit [ENTER] **OR** select "Package Control: Install Package"
[![screenshot_install_package](/images/ctrl-shif-p-install-package.png)](/images/ctrl-shif-p-install-package.png)
3. type or select "Terminus"
[![select_terminus_package](/images/ctrl-shift-p-install-enter-terminus-enter.png)](/images/ctrl-shift-p-install-enter-terminus-enter.png)

## Windows ONLY
Skip the next two sections if you are on a Mac or Linux PC, because you already have Linux and Bash configured as your default shell.

#### Windows ONLY: Install Linux in Sublime
Microsoft's instruction's are here: https://learn.microsoft.com/en-us/windows/wsl/install but you should be able to install WSL from within Sublime Text:
1. hit [ctrl-shift-P] **OR** select "Command Pallette" in the "Tools" menu (SCREENSHOT)
2. type "terminusopen" and hit [ENTER] **OR** select "Terminus: Open Default Shell in Panel"
3. click into the teminal panel at the bottom
4. type "wsl --install" [ENTER]
5. type "exit" [ENTER] to close the Terminus panel

#### Windows ONLY: Connect Terminus to Linux (WSL)
1. hit [ctrl-shift-P] **OR** select "Command Palette" in the "Tools" menu (SCREENSHOT)
2. type "terminus setting" and hit [ENTER] **OR** select "Preferences: Terminus Settings"
3. click into the left-hand side of the Sublime window with a pair of settings files
[![copy terminus settings default config section](/images/ctrl-shift-p-terminus-settings-new-window-select-copy-default-config-section.png)](/images/ctrl-shift-p-terminus-settings-new-window-select-copy-default-config-section.png)
4. copy the section at the top of the "Terminus/Terminus.sublime-settings" tab on the left:
```json
{
    // the default config, either a single config name or a platform
    // specific dict
    "default_config": {
        "linux": null,    // login shell
        "osx": null,      // login shell
        "windows": "Command Prompt"
    },
```
5. Paste the copied text into the right-hand tab for the "User/Terminus.sublime-settings" file
6. Add closing curly braces "}" to the end of your "User/Terminus.sublime-settings" file
7. Change ``"windows": "Command Prompt"`` to ``"windows": "WSL Login Shell"``
[![change Command Prompt to WSL Login Shell](/images/sublime-terminus-settings-changed-default-config-WSL.png)](/images/sublime-terminus-settings-changed-default-config-WSL.png)
8. Hit [ctrl-S] key to save your Terminus settings
9. Click the [X] on the Sublime Text Window or type [ctrl-Q] to close you Terminus settings files

## Bashy game within Sublime (Assignment)

1. hit [ctrl-shift-P] **OR** select "Command Palette" in the "Tools" menu (SCREENSHOT)
2. type "terminusopen" and hit [ENTER] **OR** select "Terminus: Open Default Shell in Panel"
3. click into the terminal in the bottom of your screen
4. type "pwd" [ENTER]
5. type "ls" [ENTER]
6. type "mkdir adventures" [ENTER]
6. type "cd adventures" [ENTER]
7. type "touch username.py" [ENTER]
8. screenshot and upload your Sublime Text window so I can see that you successfully created a py-file within your ``code`` folder

