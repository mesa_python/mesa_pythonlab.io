---
title: 'Random walking purple turtle'
description: 'This post should help you solve the random walking purple Turtle exercise in Runestone, Foundations of Python Programming, Chapter 7.'
pubDate: 'Feb 28, 2024'
---
# Random walking purple turtle
This post should help you solve the random walking purple Turtle exercise in Runestone, Foundations of Python Programming, Chapter 7.

### Sidenote for Linux users
On Linux, you will likely need to add the Tkinter (tk) package to python.
It's used to create Graphical User Interfaces (GUI) like the windows that `Turtle` uses to display line drawings in the `Turtle` module.

```
$ sudo apt install python3-tk
```

## Using Turtle to draw a mouse
Just as in all the other exercises, you need to create `Screen` and `Turtle` objects before you start drawing.
Call your turle mickey to match the code in Runestone.

```python
>>> import turtle
>>> turtle
<module 'turtle' from '/usr/lib/python3.10/turtle.py'>
```

```python
>>> wn = turtle.Screen()
>>> wn
<turtle._Screen at 0x7fc414e2fc10>
>>> ted = turtle.Turtle()
>>> ted
<turtle.Turtle at 0x7fc4163f93f0>
```

I like how Python prints out the type or class name for your objects whenever you print them to the console.

Here's the sample python program that was given in the problem statement:

```python
>>> import turtle

>>> ted = turtle.Turtle()
>>> ted.speed(10)

>>> for _ in range(36):
...     ted.stamp()
...     ted.forward(15)
```

Specifying colors with RGB tripples can be tricky.
Trial and error is probably the best way to find the color you want quickly.
You could also find an RGB color chart on the web, if you need something pretty precise.

```python
>>> ted.fillcolor((.7,0,1))  # red+blue=>cyan rather than purple, so don't use (1,0,1)
ted.pencolor((.7,0,1)) # for purple you will need more blue than red

Here's how I did the task, from start to finish:

```python
import turtle
import random

ted = turtle.Turtle()
ted.speed(10)
ted.fillcolor((.7,0,1))  # red+blue=>cyan rather than purple, so don't use (1,0,1)
ted.pencolor((.7,0,1)) # for purple you will need more blue than red

for i in range(36):
    ted.stamp()
    dist = random.randint(5, 26)
    ted.penup()
    ted.forward(dist)
    ted.pendown()
```
