---
title: 'Runestone instructor workflow'
description: 'Runestone instructor workflow.'
pubDate: 'Jan 23, 2024'
---

# Runestone

## Instructor workflow

---

## Week 0

1. **Register and select existing _book_ name**
2. Create a course 
3. Create assignments
4. Create exam(s)

---

## `Runestone.academy` home page

[![Runestone.Academy home Page](/images/runestone-homepage.png)](https://runestone.academy/ "Runestone.Academy home page")

---

## Runestone.Academy instructor workflow

#### Step 2
![runestone-instructor-02.png](/images/runestone-instructor-02.png "Runestone.Academy instructor workflow - step 2")

---

## Runestone.Academy instructor workflow

#### Step 3
![runestone-instructor-03.png](/images/runestone-instructor-03.png "Runestone.Academy instructor workflow - step 3")

---

## Runestone.Academy instructor workflow

#### Step 4
![runestone-instructor-04.png](/images/runestone-instructor-04.png "Runestone.Academy instructor workflow - step 4")

---

## Runestone.Academy instructor workflow

#### Step 5.1
![runestone-instructor-05.1.png](/images/runestone-instructor-05.1.png "Runestone.Academy instructor workflow - step 5.1")

---

## Runestone.Academy instructor workflow

#### Step 5.2
![runestone-instructor-05.2.png](/images/runestone-instructor-05.2.png "Runestone.Academy instructor workflow - step 5.2")

---

## Runestone.Academy instructor workflow

#### Step 5.3
![runestone-instructor-05.3.png](/images/runestone-instructor-05.3.png "Runestone.Academy instructor workflow - step 5.3")

---

## Runestone.Academy instructor workflow

#### Step 5.4
![runestone-instructor-05.4.png](/images/runestone-instructor-05.4.png "Runestone.Academy instructor workflow - step 5.4")

---

## Runestone.Academy instructor workflow

#### Step 5.5
![runestone-instructor-05.5.png](/images/runestone-instructor-05.5.png "Runestone.Academy instructor workflow - step 5.5")

---

## Runestone.Academy instructor workflow
#### Step 5.6
![runestone-instructor-05.6.png](/images/runestone-instructor-05.6.png "Runestone.Academy instructor workflow - step 5.6")

---

## Runestone.Academy instructor workflow
#### Step 5.7
![runestone-instructor-05.7.png](/images/runestone-instructor-05.7.png "Runestone.Academy instructor workflow - step 5.7")

---

## Runestone.Academy instructor weekly task 1
#### Automaticly monitor student textbook interactions:
- Textbook section views (reading)
- Embedded video playback starts 
- In-context question attempts
- Number correct answers

![Monitor reading progress](/images/runestone-instructor-06.png)

---

## Runestone.Academy instructor weekly task 2
#### Graded assignments in Runestone
Creat a graded assignment each week (chapter) by selecting "timed".
The time limit is optional.

![Create timed assignment to record autograde results](/images/runestone-create-timed-assessment.png)

---

## Runestone.Academy instructor weekly task 3
## Download gradebook as CSV

All grading (autograde button) results can be Downloaded as a CSV (spreadsheet).
Must transfer grades to Canvas manually or use Python and Canvas API to upload grades.

![Download grades](/images/runestone-grades-download-csv.png)

