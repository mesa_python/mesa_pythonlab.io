---
title: 'Runestone assignments modules 9 and 10 (chapter 12-15)'
description: 'Modules 9 and 10 cover tuples (chapter 12), functions (chapter 13), advanced iteration (chapter 14), and advanced functions (chapter 15).'
pubDate: 'April 10, 2024'
---

## 9.1 Overview

This week you will be learning about functions (Chapter 12) and tuples (Chapter 13).

### 9.2 Reading - Chapter 12 - functions

Functions are a great way to modularize your code and avoid having to copy and paste code in multiple places.
Learn more about function on Runestone with the ["Module 9.2 (Chapter 12) - Functions"](https://runestone.academy/runestone/lti?assignment_id=178579) reading assignment and Python problems.

### 9.3 Reading - Chapter 13 - tuples

Pack and unpack tuples in chapter 13 in runestone [Module 9.3 Chapter 13 - tuples](https://runestone.academy/runestone/lti?assignment_id=177586)

## 10.0 Overview

In module 10 you will learn about advanced iteration and 


## 10.2 Reading - Chapter 14 - More About Iteration

This week you will learn how to create a `while` loop.
You can use a `while` loop to listen for input from your user or any external system.
This is called the _Listener Loop_ pattern.
It's especially useful in video games, web applications or application where external events trigger changes in your program.
I bet you'll think of uses for the while loop in your data-driven text adventure program for the final project.

In both `for` loops and `while` loops you will often want to break out of the loop with a `break` statement, or skip the remaining lines of code in the loop using a `continue` statement. 

To learn about the `while`, `break` and `continue` keywords, checkout out Chapter 14 in Runestone.
You'll get to practice your advanced iteration skills as you read the chapter and complete the interactive exercises embedded in the text.

#### Table of contents

- Chapter 14. More About Iteration
  - 14.1 Introduction
  - 14.2 The while Statement
  - 14.3 The Listener Loop
  - 14.4 Randomly Walking Turtles
  - 14.5 Break and Continue
  - 14.6 👩‍💻 Infinite Loops

## 10.3 Exercises - Chapter 14 - More About Iteration

Test your advanced iteration skills with the exercises (quiz) in the Runeston assignment named ["Module 10.2 (Chapter 14) - Advanced Iteration"](https://runestone.academy/runestone/lti?assignment_id=178579)
The exercises are due 2024-04-14 23:00 Pacific time

#### Questions
- "[ac14\_10\_1](https://runestone.academy/runestone/admin/assignments#component-preview)"
- "[ac14\_10\_2](https://runestone.academy/runestone/admin/assignments#component-preview)"
- "[ac14\_10\_3](https://runestone.academy/runestone/admin/assignments#component-preview)"
- "[ac14\_10\_4](https://runestone.academy/runestone/admin/assignments#component-preview)"
- "[ac14\_10\_5](https://runestone.academy/runestone/admin/assignments#component-preview)"
- "[ac14\_10\_6](https://runestone.academy/runestone/admin/assignments#component-preview)"

## 10.4 Exercises - Chapter 15 - Reading

## 10.4 Exercises - Chapter 15 - Reading


## Module 11.2



## Module 11.3

Nested data and iterating through it. Saving and loading json?

Due: 2024-04-21

https://runestone.academy/runestone/lti?assignment_id=178582


## Module 13.1

### Create a data-driven text-adventure

"Data-driven" is a computer science concept of parameterizing your program so that it can be controlled or driven by a separate source of data other than the program itself.
In most cases this means that you want to load a text file containing structured data.
Some examples of text files containing data are here:

- syllabus_