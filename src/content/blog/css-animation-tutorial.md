---
title: 'Pure HTML+CSS annimation'
description: "You can create a simple frame-by-frame stop motion animation without javascript once you learn a few new HTML and CSS keywords!"
pubDate: 'Oct 1, 2024'
---

# Pure HTML+CSS annimation

You can create a simple frame-by-frame stop motion animation using only HTML and CSS!

Here's a rectangle, fading into existing using the `@keyframes` and `animation` keywords in CSS. All you need in your HTML is a class name for the container holding your animation.

### Fade in a rectangle
```html
<div class="container-fader"></div>
<style>
@keyframes fadeIn {
  0% { opacity: 0; }
  100% { opacity: 1; }
}
.container-fader {
  animation: fadeIn;
  animation-duration: 2s;
  width: 100px;
  height: 100px;
  border-radius: 20%;
  padding: 20px;
  background: #6E56FD;
}
</style>
```

### Loop a fade-in circle forever

A few of the most common animation attributes can be set with positional argument shortcuts on the `animation:` attribute.

And you can turn a rectangle into a circle by rounding it's corner with 50% or greater radius.

Of course the `background:` color is how we're filling our circle so it is visible on the white background of the parent `<HTML>` or `<BODY>` element.

```html
<div class="container-fader"></div>
<style>
@keyframes fadeIn {
  0% { opacity: 0; }
  100% { opacity: 1; }
}
.container-fader {
/*  animation: name   duration timing-function delay iteration-count direction fill-mode; */
    animation: fadeIn 2s       linear          1s    infinite;  
    /*
    animation: fadeIn;
    animation-duration: 2s;
    animation-delay: 1s;
    animation-iteration-count: infinite;
    */
    width: 100px;
    height: 100px;
    /* radius of rectangular border rounded corners. >=50% creates a circle */
    border-radius: 50%;
    padding: 20px;
    background: #104010;  /* dark green */
    /* background: #208020;  /* green */
    position: relative;
}
```

### Resources

[^1]: https://blog.replaybird.com/css-keyframe-animations/
[^2]: [walking bird demo](https://codingartistweb.com/2022/06/css-animation-tutorial/#css-animation-tutorial)