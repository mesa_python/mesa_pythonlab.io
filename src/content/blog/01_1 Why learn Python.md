---
title: '1.1: Why learn Python'
description: 'Python is a prosocial language designed to be easy for both your and other humans to read.'
pubDate: 'June 1, 2024'
---

# 1.1: Why learn Python (Watch)
___


"You primarily write your code to communicate with other coders, and, to a lesser extent, to impose your will on the computer." -- Guido van Rossum [^1]

Python is a prosocial language!
It is designed to be easy for both your and other humans to read so they can tell what you program does.
Readability makes Python code reusable by others.
This helped Python go viral from the very beginning.
People started creating open source Python programs for their friends to use and share.
Python quickly overtook all other programming languages as the most popular language in the world.
In this lesson you will learn about all these things that people are using Python for.

#### What makes Python is so popular and so powerful
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/oAx4YfQatws?si=Upqh3vzJRr61m5-S" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen=""></iframe>


## Python is designed for readablity!

Guido van Rossum invented Python because he thought other languages were unnecessarily complicated and hard to read.
And he understood the importance of using a readable, concise language when you are trying to explain to someone (or some machine) what you want them to do.
If you both understand what the words mean that will save a lot of work.

"The ratio of time spent reading versus writing is well over 10 to 1. We are constantly reading old code as part of the effort to write new code. ...[Therefore,] making it easy to read makes it easier to write." -- Bob Martin (author of _Clean Code_) [^2]

The core developers of Python use English words, punctuation, and abbreviations in ways that will make sense to you.
They chose English action verbs like "help" and "print" that tell you what the code will do.
And Python has interactive environment where you can try out Python statements to see if they do what you expect.
Here's an example command that shows how you can get help, right inside a Python session.

Don't worry if this doesn't make sense to you yet, but I can bet you understand what `help(print)` might do.

```python
>>> help(print)
Help on built-in function print in module builtins:

print(...)
    print(value, ..., sep=' ', end='\n', file=sys.stdout, flush=False)
    Prints the values to a stream, or to sys.stdout by default.
    Optional keyword arguments:
    file:  a file-like object (stream); defaults to the current sys.stdout.
    sep:   string inserted between values, default a space.
    end:   string appended after the last value, default a newline.
    flush: whether to forcibly flush the stream.
```

The built-in `help()` function is what you use when you want to see the documentation on any object in Python.
_Play around_ with words like this in the Runestone.Academy exercises this week!

## Spaces matter!

In Python you need to lay out your code using the `[SPACEBAR]` and other _whitespace_ in order for it to work correctly.
Whitespace characters are the invisible letters that are created in a text file when you hit the `[SPACEBAR]`, `[TAB]`, or `[ENTER]` keys on your computer keyboard.
They are called whitespace, because on a piece of white paper you wouldn't see any letter at all, it would just look like white space between letters.

If you write more than one line of Python code you need to think about how you want it to run.
If you want to create a section of code you need to _indent_ it using the `[SPACEBAR]`.
This makes your code look like an outline!
And outlines are much easier to read and understand than a wall of text.
And you need to use 4 or 8 spaces to indent everything in your Python programs for this course.
Don't use 1, 2 or 3 spaces for Python code.
That will just confuse you (and your computer).
Remember, Python is about readability.
If you want a programming language that doesn't use whitespace this way, check out some unreadable languages like minimized Javascript code or Ruby.

Using the space character rather than the `[TAB]` characterThis makes sure your code will look the same no matter what operating system someone is using to look at your code.
You will often find yourself hitting the `[SPACEBAR]` four times in a row, and it may seem like a pain.
In later modules you will learn tricks to indent your code with 4 or 8 spaces or whatever you need, with only one keystroke.
But for now, just force yourself to use the spacebar.

## What can you do with Python?

In the video you learned about some famous Python programs that are powering rovers on Mars or making billionaires richer on big tech social media like Instagram.

Here are some more famous uses for Python:

- Star Wars computer-generated graphics
- iRobot's Roomba vacuum
- Reddit website
- NASA uses it everywhere in the Solar System and beyond
- Netflix website and recommendations
- Google's YouTube and search engine
- Spotify music service
- Tesla self-driving cars
- ChatGPT and all AI systems

These are all hidden proprietary programs, but it turns out there are many open source alternatives that you can use to do much of the same stuff with Python:

- [RobotFramework](https://robotframework.org/) - open source system
- [Mastodon]() - social network similar to Xitter
- [Flashcards](pypi.org/project/flashcards) - study for class
- [YouTubeDL](https://github.com/ytdl-org/youtube-dl) - downloader for YouTube videos
- RobotOS - an operating system that runs Python on bots like Roomba
- Comma.ai - an open source self-driving car add-on for most modern cars
- Spottube & [SpotDL](https://pypi.org/project/spotdl/) - download Spotify and YouTube songs and playlists

But what can _YOU_ build with Python?

#### HINT FOR DISCUSION ASSIGNMENT
You can search GitHub for ["beginner Python projects"](https://github.com/topics/python-projects) to find out what others like you are doing with Python.

Probably one of the smallest useful programs you might write will be in a Spreadsheet.
LibreOffice (the free alternative to MS Office and Google Docs) lets you run Python code right within the cells of a spreadsheet.
Another popular application for Python is building "command line" applications, like the ones you will build for this course.
A command line application is a command you can type in a terminal shell (like git-bash or Powershell) to do something for you.

Here are some of the command line apps you could build:

- games - text adventures are fun to build and don't require much experience
- reminders - send a reminder whenever you have something you need to do
- search - search for files on your computer
- send e-mails - send bulk or individual e-mails to yourself and your friends
- browse the web - you can download text from the web and filter out the garbage
- post to social media - you can post to social media like Mastodon
- website - Python packages like Flask make it possible for you to build a website

You'll get a chance to brainstorm with your fellow students about fun things to do with Python in _this week's discussion_. 

___

#### Footnotes
[^1]: [The Mind at Work: Guido van Rossum on how Python makes thinking in code easier](https://blog.dropbox.com/topics/work-culture/-the-mind-at-work--guido-van-rossum-on-how-python-makes-thinking) 

[^2]: _Clean Code_ by Robert C. Martin.

