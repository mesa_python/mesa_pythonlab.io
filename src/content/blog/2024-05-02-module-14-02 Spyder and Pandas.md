---
title: '14.2 Intro to Spyder and Pandas'
description: 'Feedback and grades on your midterm project.'
pubDate: 'May 2, 2024'
---

This week you will learn how to install and run two Python packages that will be helpful for building your data-driven text adventure:

1. Spyder IDE (Integrated Development Environment) - for writing Python
2. Pandas - for working with data

### 14.2.1 Spyder

When you installed Anaconda, you also installed the Spyder IDE.
This is probably your best option for editing and testing Python scripts on a Windows system.
On Linux and Mac, you might prefer Sublime Text.
Both tools allow you to edit your Python code with state-of-the-art efficiency.

### 14.2.2 Pandas

You can install Pandas the same way you install most other Python packages, with `pip install pandas` from the command line.
You can find more information about Pandas by searching on https://pypi.org/ and searching for the keyword 'pandas'.
And if you are searching for tutorials online about how to work with data using Pandas, you probably want to add the term `pd.DataFrame` to your search query.
Most people abbreviate the name of their imported Pandas package as `pd`.
And a Pandas `DataFrame` is like a combination of a dictionary of dictionaries (one dictionary per column) and a list of lists (2-d array).
This means that Pandas is a great tool for working with tabular data with columns and rows.
It's great for linear algebra and storing data similar to how a database stores tables of data.

### Video

To see Spyder and Pandas in action, watch this 30 min video.

<video controls width="320">
  <source src="https://www.dropbox.com/scl/fi/nxniwxmf3h4uxixny47b7/2024-05-02-module-14-02-video-intro-to-spyder-and-pandas.mp4?rlkey=9uxjt8a9fbr707rstfbmg36e6&st=ewv57k46&raw=1" type="video/mp4" />
  Download the <a href="https://www.dropbox.com/scl/fi/nxniwxmf3h4uxixny47b7/2024-05-02-module-14-02-video-intro-to-spyder-and-pandas.mp4?rlkey=9uxjt8a9fbr707rstfbmg36e6&st=ewv57k46&raw=1">MP4</a> video.
</video>


