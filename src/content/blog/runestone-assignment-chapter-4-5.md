---
title: 'Module 3: Runestone chapter 4-5 assignment'
description: "In chapter 4 and 5 of _Foundations of Python Programming_ you will import and run Python modules such as `random` and `turtle`. "
pubDate: 'Feb 6, 2024'
---

# Module 3: Chapter 4-5 assignment

Head over to Runestone and try your hand at importing Python modules, generating pseudorandom numbers with the `random` module, and creating line plots with `turtle`. programming language: [Module 3: 1 (Chapter 4 and 5)](https://runestone.academy/assignment/student/doAssignment?assignment_id=174380)