---
title: 'Module 4: Runestone chapter-6-7 assignment'
description: "In chapter 6 and 7 of _Foundations of Python Programming_ you will practice slicing sequences and iterating through sequences with for loops."
pubDate: 'Feb 6, 2024'
---

# Module 4: Runestone chapter-6-7 assignment

Head over to Runestone and practice using `for` loops to iterate through sequences: [Module 4 (Chapter 6 and 7)](https://runestone.academy/assignment/student/doAssignment?assignment_id=174898)