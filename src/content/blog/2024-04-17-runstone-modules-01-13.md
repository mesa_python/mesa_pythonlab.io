---
title: 'Modules 01-13 Runestone Assignments'
description: 'Chapters 1-20 in Foundations of Python Programming on Runestone.Academy.'
pubDate: 'April 17, 2024'
---


1. Module 01 (Chapter 01) - Ch1: Python programming, natural language vs programming languages, computers and operating systems. https://runestone.academy/assignment/student/doAssignment?assignment_id=172579
2. Module 02 (Chapter 2 & 3) - Ch2: Python variables, expressions, and programs that mulitply or add variables. Ch3: debugging syntax or runtime errors. https://runestone.academy/assignment/student/doAssignment?assignment_id=173803
3. Module 03 (Chapter 4 & 5) - Ch4: Import and run Python modules such as `math`, `random` and `turtle`. Ch5: Display line plots and shapes with the `turtle` module. https://runestone.academy/assignment/student/doAssignment?assignment_id=174380
4. Module 04 (Chapter 6 & 7) - Ch6: Sequences (`list`s). Ch7: Iteration using `for` loops and sequences. https://runestone.academy/assignment/student/doAssignment?assignment_id=174898
5. Midterm Exam (Chapters 1-8) - programming and debugging, variables, variable types, order of operation, error types, sequences, indexing and iteration, conditions. https://runestone.academy/assignment/student/doAssignment?assignment_id=172577
6. Module 06 (Chapter 8 & 9) - Ch8: Conditional expressions and binary `bool` operators (`and`, `or`, `in`).  Ch9: transforming a sequence (`list` or `str`) and the accumulator pattern. https://runestone.academy/assignment/student/doAssignment?assignment_id=175742
7. Module 07 (Chapter 21 reading) - Read chapter 21 to learn how to design and build your midterm project Python program. https://runestone.academy/assignment/student/doAssignment?assignment_id=179490
8. Module 08 (Chapter 10 & 11) - Practice reading, writing, and processing files, including CSV files; hash table (`dict` type) data structure. https://runestone.academy/assignment/student/doAssignment?assignment_id=177413
9. Module 09 (Chapter 12 & 13) - Ch12: Defining functions. Ch13: Packing and unpacking tuples. https://runestone.academy/assignment/student/doAssignment?assignment_id=177586
10. Module 10 (Chapter 14 & 15) - advanced iteration and functions (optional named keyword arguments \*args and \*\*kwargs). https://runestone.academy/assignment/student/doAssignment?assignment_id=178580
11. Module 11 (Chapter 16 & 17) - Ch16: Sorting lists and dictionary keys. Ch17: Iterating through nested data and `get`ing values with indexing (`[]`). https://runestone.academy/assignment/student/doAssignment?assignment_id=178582
12. Module 12 (Chapter 18 & 20) - test cases and object oriented programming (defining classes and instantiating objects). https://runestone.academy/assignment/student/doAssignment?assignment_id=179488
13. Module 13 (Chapter 1-17) - Final Exam. https://runestone.academy/assignment/student/doAssignment?assignment_id=172578