---
title: 'Module 1: Runestone chapter 1 assignment'
description: "In chapter 1 of _Foundations of Python Programming_ you will practice your understanding of what a programming language is and what it's used for."
pubDate: 'Feb 6, 2024'
---

# Module 1: Runestone Chapter 1 assignment

Head over to Runestone and try your hand at these questions about the Python programming language: [Module 1 (Chapter 1)](https://runestone.academy/assignment/student/doAssignment?assignment_id=172579)