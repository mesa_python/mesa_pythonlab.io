---
title: 'Module 11 - Games for teaching and learning computer science'
description: 'Games for learning Python, Bash, git, and vim'
pubDate: 'April 9, 2024'
---

For module 11 you need to start thinking about your final project, a data-driven text adventure game.
The games below can help you hone your advanced computer skills and maybe give you ideas for your text adventure.
Bonus points if your game includes some Python questions or commands in the winning "path."

### 1. Files and directories

#### [Bashy](http://bruab.github.io/bashy/)
A web-based game ([javascript on GitHub](http://bruab.github.io/bashy/)) by Brian David Hall for learning `ls`, `cd`, `cat`, and `grep`. Shows a tree of the directories in the game to help you navigate.

### 2. Editors

If you are able to edit text files using `nano` then you probably don't need to learn everything about vim.
But everyone should know how to use vim, in a pinch, because it is the default command line text editor on most Linux systems and applications (like `git`).
If nothing else, make sure you know how to launch `vim` or `vi`, edit a file, save it, and exit `vim`. 
Getting out of `vim` can be a find-the-secret-door game, all by itself.

#### [OpenVim](http://www.openvim.com/tutorial.html)
Interactive tutorial for complete VIM beginners.

#### [VimGolf](http://vimgolf.com/)
Command line and website game to hone your vim skills.

#### []()


#### Secret Adventure (emacs)
If you want to have a little fun within the emacs editor, there's  built-in game called "Secret Adventure."
Debian doesn not come with emacs preinstalled, but you can install it with `apt install emacs`.
Then, to launch emacs and the text adventure game: `emacs -batch -l dunnet`

### 3. Git

#### [learnGitBranching](http://pcottle.github.io/learnGitBranching/) 
Thorough interactive web app that covers beginning and intermediate Git commands with beautiful visualizations and encouraging feedback.

### 4. Algorithms and automation

#### https://github.com/AlgoTaurus/algotaurus
Solve a maze with your algorithm. AlgoTaurus has top-down map of where you are in the maze and a line-by-line debugger for seeing how each line of your algorithm.

<!-- #### https://turingcomplete.game/

#### australia data science education inst. mciver

#### search https://scribe.rip/p/what-every-software-engineer-should-know-about-search-27d1df99f80d

#### eulerproject

-->
### 10. AI

#### [Google AI Challenge](https://github.com/aichallenge/aichallenge) 
An old 2011 competition to design bots (AI agents) that could control an ant colony in competition with other bots.
Like AlgoTaurus, you get to watch a top-down view of the ant battles and exploration.

#### [Colobot](https://colobot.info/)



## Just for fun

These are fun games for the terminal, but they have ASCII Art-displays (remember Rogue?).

#### [2048](https://github.com/gabrielecirulli/2048)
To play, you must slide blocks up, down, left, and right to combine (add) matching pairs and increment numbers, until you've made a block that is 2048 in size. 
You'll get really good at factors of two (2, 4, 8, 16, 32, ...).
The catch, is that you can't just move one block, you can only move every block on the screen, provided it has space to slide.
To install on fedore do `sudo dnf install 2048-cli`, on Debian (Ubuntu) `sudo apt install 2048`.
On Debian-based systems you can also install a graphical version with `sudo apt install 2048-qt`.
Whatever your OS package manager is, you can probably search for 2048 and find a couple versions that will work for you.
To install you need to use git to clone and build the GitHub repo, so make sure you've already mastered learnGitBranching.
If you're on fedora then 
The original is FOSS by Marc Tiehuis, written in C (MIT license) [on GitHub](https://github.com/tiehuis/2048-cli).


#### [Pacman4Console](https://github.com/YoctoForBeaglebone/pacman4console) a [Pac-Man clone](https://en.wikipedia.org/wiki/Pac-Man).
You can install one of the Pac-Man clones on your system with `dnf install myman` or `apt install pacman` or something similar.


#### Nudoku (Sudoku from the command line)

Nudoku is a  sudoku game that you can play in the terminal.
The [C source code is on GitHub](https://github.com/jubalh/nudoku).

#### Snake

#### Snake
Snake was called ["Nibbles"](https://en.wikipedia.org/wiki/Nibbles_(video_game) in the 90s, and came installed with QBasic.
A recent version of [Snake is written in Go](https://github.com/DyegoCosta/snake-game).
On BSD you can find one in the game pack called [bsdgames](https://github.com/vattam/BSDGames).

And you can run it in a container if you aren't ready to go with a Go compiler (GoLang): `docker run -ti dyego/snake-game`.


#### Tetris
Tetris was wildly popular as far back as the 80's.
Sam Tay's [version](https://github.com/samtay/tetris) is [written in Haskell,](https://github.com/samtay/tetris) and it's one of the better-done versions I've seen, with on-screen preview, score, help, and a clean look.

If you're willing to run a compiled binary from an untrusted source (I wouldn't recommend it), you can grab that directly, but for a safer approach, it's also easy to use a containerized version with [dex](https://github.com/dockerland/dex), or to install from source with [stack](https://docs.haskellstack.org/en/stable/README/#how-to-install).

This particular Tetris clone is by Sam Tay and available under a BSD license. [Check it out](https://github.com/samtay/tetris)!