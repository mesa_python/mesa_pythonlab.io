---
title: 'Announcement: Module 13 Final Exam in Runestone'
description: 'The exam time limit is 120 minutes and is due on Saturday April 26th at 11 pm, though you can submit your exam late for full credit.'
pubDate: 'April 24, 2024'
---


# Announcement: Module 13 - Final exam

Once you click the "Start" button in Runestone ["Module 13: Final Exam"](https://runestone.academy/assignment/student/doAssignment?assignment_id=172578), you have 120 minutes to answer all the questions.
Make sure your laptop is plugged in so you don't lose your work and start over.
Do not restart your browser or your computer during one exam session.
If you pause the exam for too long, or accidentally clear the cookies for Runestone, you may lose your work and have to start over.
You may restart the exam as often as you like.

The exam, like the homework, is open book.
You may refer to any of the resources you've been using for this class:

+ The Runestone textbook [Foundations of Python Programming](https://runestone.academy/ns/books/published/mesa_python/index.html)
+ A Python interpreter - use `help()` or run Python code snippets
+ Jupyter notebook
+ Python (or iPython) console
+ Your IDE (Spyder, Sublime, VSCode)
+ The GitLab repository for this course
+ Wikipedia
+ Terminal (shell) commands - `man`, `grep` and `python`

Feel free to use a Jupyter notebook, Python console or your text editor (IDE) as a **scratch pad** for your answers.
I've found two advanced questions about "Big O Notation" and "Binary Search" that are not explained anywhere in the textbook, so I will show you how to answer these questions in two videos for [Module 11 (Chapter 16 & 17)](https://sdccd.instructure.com/courses/2467702/modules/5234100).



 