---
title: 'Getting Started with Runestone'
description: 'Runestone student workflow.'
pubDate: 'Jan 29, 2024'
---

## Week 1

1. **Log in to your account**
2. Chapter 1 reading comprehension exercises 
3. Answer questions within textbook
4. Discuss chapter in Canvas

---

## `Runestone.academy` Home Page

- Visit "runestone.academy" in your browser
- Click the **Log In** button in the top right-hand corner

[![Runestone home page with login button at top of the page](/images/canvas-runestone/runestone-student-01-login.png)](https://runestone.academy/user/login?_next=/ns/books/published/mesa_python/index.html)

---

## Username and password

- Your username is **your last name**, in **lowercase** characters with the number **24** at the end.
- My username is **lane24**.
- You password is the last **6 digits** of your **student ID**.

[![Login form in upper right with username 'lane24' and a 6-char password filled in.](/images/canvas-runestone/runestone-student-02-login-form-lane24-6char-pw.png)](https://runestone.academy/user/login?_next=/ns/books/published/mesa_python/index.html)

<!--
---

## Donation
- You may see a donation request nag page when you first sign in.
- Click the "Sorry, not today!" button. 
![Runestone donation page suggests $10, $20, ... donations by PyPal, Venmo, or Card](/images/runestone-donation-not-today-button.png "Donation request")
-->

---

## Change your password!!!

- Find the pulldown menu at the upper righthand corner.
- Click on the "Change password" menu item. 

![Menu showing change password, assignments, and other pages.](/images/canvas-runestone/runestone-student-04-1-change-password-menu-item.png)

---

## Choose a password you will remember

![Change password form](/images/canvas-runestone/runestone-student-04-2-change-password-form.png)

---

## Find your weekly assignments

- Log in again with your new password
- Find the **Assignments** menu item

![Pulldown menu at upper right](/images/canvas-runestone/runestone-student-05-0-assignments-menu-item.png)

---

## Week 1 assignment
- Click on the Module 1 assignment link.

![Module/week 1 assignment in a list](/images/canvas-runestone/runestone-student-05-1-assignment-list-module1.png)

---

## Reading assignments
- Click on the first reading assignment

![](/images/canvas-runestone/runestone-student-05-2-assignment-read-1.1.png)

---

## Videos
- Play the first video at the beginning of chapter 1

![](/images/canvas-runestone/runestone-student-06-play-first-video.png)

---

## Inline programming challenges
- Some questions will ask you to type Python code in the browser 

![Programming challenges](/images/canvas-runestone/runestone-student-07-checks-inline-programming-answers.png)

---

## Textbook table of Contents

You can always find the textbook by clicking on [**mesa_python**](https://runestone.academy/ns/books/published/mesa_python/index.html) in the upper left

[![Foundations of Python Programming table of contents](/images/runestone-student-10-mesa_python-textbook-toc.png)](/images/runestone-student-10-mesa_python-textbook-toc.png)