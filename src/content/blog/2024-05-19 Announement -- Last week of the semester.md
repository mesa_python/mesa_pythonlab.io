---
title: '16_0 Announcement: Final project due today'
description: 'Week 16 final project due today, but you have until May 25.'
pubDate: 'May 15, 2024'
---

The final project was due today, the end of the 16th weekly module for this course. Nonetheless, the last day of the semester is Saturday, May 25th at 10 PM. Whatever assignments you have turned in by then will be counted in full towards your final grade. I am available to help you all week. Use this link to put a 30-minute meeting on my calendar here: https://calendly.com/hobs

The sooner you turn things in, the sooner you can get feedback and potentially increase your grade by improving on your assignments. It usually takes me about 2 days to grade a programming project and get feedback to you, so plan ahead.

Here are some tips that helped the students who have turned in final projects and earned an A:

- You will lose points if you do not comply with the PEP8 style guide: https://peps.python.org/pep-0008 . PEP8 is a requirement in most workplaces where developers collaborate on software development. And it is mentioned in the instructions for both project assignments.
- You can install and use a linter plugin within your IDE (Spyder, Sublime Text, IDLE, PyCharm, VSCodium) to detect and highlight your syntax errors as well as PEP8 style mistakes. The following python packages can be `pip install`ed and used on the command line: `pylint`, `flake8`, `pyflakes`.
- Autolinters such as `black` (`pip install black`) will clean up your code for you with the command `black .` The dot at the end of the command means to clean up all files in the current directory.
- Use string processing methods such as `.strip()`, `.split()`, `.lower()`, `.replace()`, `.startswith()`, `.endswith()`, `in`, slices (`[:2]`) and indexing (`[0]`) to make your conditional expressions on input text more "forgiving" of user typos and abbreviations.
- Use the `help()` function built into the interpreter to learn about any function or data type or class in the Python console. For example, try `help(str)` to see all the methods and attributes of `str` objects.
- Use the [TAB] key after typing as much as you can remember of an object's name to see a list of possible completions. For example, type `str.s` and then hit the [TAB] key to see possible completions like `str.split`, `str.strip`, `str.swapcase` and `str.startswith`. This will work on any variable, whether it contains an object or a function. Everything in Python is an object.
- Use the `??` operator at the end of an object to display its source code and learn "best practices" for writing clean Python code. This works best for imported objects and modules. For example: `import random` and then `random??`.