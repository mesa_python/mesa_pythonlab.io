---
title: 'Syllabus corrections and improved communication'
description: 'Appology for poor communication and improvements underway.'
pubDate: 'April 17, 2024'
---

Thank you very much to those students that worked together to consolidate your feedback.
This will help me deal with your feedback efficiently, all at once.

I'm very sorry for my poor communication, and errors in the syllabus and grading.
I will improve my communication and have tried to fix the Runestone assignments.
Please provide additional feedback to the school or to me if there is more I can do to help you learn Python.
These are some of the things I've improved this week.
I'll send another announcement this weekend with more improvements.

1. Communication
  - Please use Canvas Inbox for fastest response times (24-48 hours)
  - Your e-mail is very important to me, but it was getting buried in the e-mails I receive from Mesa and other clients and spam
  - Last week Google was moving most of my e-mail to Spam
  - I lost some e-mails when I exported my data from Google and moved it to ProtonMail.com (more reliable and less spam, same hobson@totalgood.com e-mail address)
  - Do NOT use Pronto
  - I have concluded another work assignment (a book manuscript), so I now have no significant projects competing with you for my attention
2. To make up for my poor communication you can get extra credit
  - Help me improve by [submitting Issues on GitLab](https://gitlab.com/mesa_python/cisc-179-spring-2024/-/issues/new)
  - _Bug bounty_: if you find bugs in a Runestone assignment you may be awarded 1% extra credit on your final grade for each GitLab Issue you submit
  - _Feature bounty_: if you think of a significant improvement to a Runestone question or an additional code challenge, or an improved explanation of a Python concept that you found difficult, you may be awarded 1% extra credit
  - Total extra credit is limited to +10% (110% total grad) for all bug and feature bounties
3. Due dates
  - The due dates in the schedule table at the [bottom of the syllabus](https://sdccd.instructure.com/courses/2467702/assignments/syllabus) has been corrected
  - All assignments are due on the Saturday night at midnight at the end of the week
  - All assignments will be accepted late (but no later than May 25)
  - The course ends on Saturday May 25th and I cannot accept any assignments after then
4. Runestone assignments for modules 1-13 have been updated and debugged
  - There is only one Runestone assignment for each canvas module
  - There are 1 or 2 chapters for each Runestone assignment (except exams)
  - There are runestone reading assignments to help you with the text adventure projects
  - You should be able to see accurate grades for every assignment
5. Runestone grades
  - I think I have fixed the Runestone grading for week 1-13 assignments
  - Week 13 is the final exam (Ch 1-17) due April 27 (but late submissions OK)
  - You will receive a grade report card by April 28 (for all assignments up to that date)
  - If you see an error on your report card, I will adjust your grade based on your feedback
  - Your report card will include feedback on your midterm project


### 13 improved Runestone assignments

1. Module 01 (Chapter 01) - Ch1: Python programming, natural language vs programming languages, computers and operating systems. https://runestone.academy/assignment/student/doAssignment?assignment_id==172579
2. Module 02 (Chapter 2 & 3) - Ch2: Python variables, expressions, and programs that mulitply or add variables. Ch3: debugging syntax or runtime errors. https://runestone.academy/assignment/student/doAssignment?assignment_id=173803
3. Module 03 (Chapter 4 & 5) - Ch4: Import and run Python modules such as `math`, `random` and `turtle`. Ch5: Display line plots and shapes with the `turtle` module. https://runestone.academy/assignment/student/doAssignment?assignment_id=174380
4. Module 04 (Chapter 6 & 7) - Ch6: Sequences (`list`s). Ch7: Iteration using `for` loops and sequences. https://runestone.academy/assignment/student/doAssignment?assignment_id=174898
5. Midterm Exam (Chapters 1-8) - programming and debugging, variables, variable types, order of operation, error types, sequences, indexing and iteration, conditions. https://runestone.academy/assignment/student/doAssignment?assignment_id=172577
6. Module 06 (Chapter 8 & 9) - Ch8: Conditional expressions and binary `bool` operators (`and`, `or`, `in`).  Ch9: transforming a sequence (`list` or `str`) and the accumulator pattern. https://runestone.academy/assignment/student/doAssignment?assignment_id=175742
7. Module 07 (Chapter 21 reading) - Read chapter 21 to learn how to design and build your midterm project Python program. https://runestone.academy/assignment/student/doAssignment?assignment_id=179490
8. Module 08 (Chapter 10 & 11) - Practice reading, writing, and processing files, including CSV files; hash table (`dict` type) data structure. https://runestone.academy/assignment/student/doAssignment?assignment_id=177413
9. Module 09 (Chapter 12 & 13) - Ch12: Defining functions. Ch13: Packing and unpacking tuples. https://runestone.academy/assignment/student/doAssignment?assignment_id=177586
10. Module 10 (Chapter 14 & 15) - advanced iteration and functions (optional named keyword arguments \*args and \*\*kwargs). https://runestone.academy/assignment/student/doAssignment?assignment_id=178580
11. Module 11 (Chapter 16 & 17) - Ch16: Sorting lists and dictionary keys. Ch17: Iterating through nested data and `get`ing values with indexing (`[]`). https://runestone.academy/assignment/student/doAssignment?assignment_id=178582
12. Module 12 (Chapter 18 & 20) - test cases and object oriented programming (defining classes and instantiating objects). https://runestone.academy/assignment/student/doAssignment?assignment_id=179488
13. **Module 13 (Chapter 1-17) - Final Exam.** https://runestone.academy/assignment/student/doAssignment?assignment_id=172578