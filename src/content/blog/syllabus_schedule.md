---
title: 'Schedule'
description: 'Weekly schedule of assignments (Runestone.Academy, projects, exams) and grade (points) for each.'
pubDate: 'Feb 13, 2024'
---

|   Week | Due Date   | Runestone   |   Points | Description                                                                            |
|-------:|:-----------|:------------|---------:|:---------------------------------------------------------------------------------------|
|      1 | 2024-02-04 | Ch 1        |        2 | Python programming language, variables and types                                       |
|      2 | 2024-02-11 | Ch 2-3      |        2 | Debugging and object oriented programming                                              |
|      3 | 2024-02-18 | Ch 4-5      |        2 | Sequences and iteration                                                                |
|      4 | 2024-02-25 | Ch 6-7      |        2 | Indexing, slicing, combining, and iterating through sequences (`list`, `str`, `tuple`) |
|      5 | 2024-03-03 | Ch 1-7      |       15 | MIDTERM EXAM - FOPP Chapters 1 - 7                                                     |
|      6 | 2024-03-10 | Ch 8-9      |        2 | Conditional expressions and modifying sequences                                        |
|      7 | 2024-03-17 |             |       15 | MIDTERM PROJECT: Text Adventure!                                                       |
|      8 | 2024-03-24 | Ch 10-11    |        2 | Files and dictionaries                                                                 |
|      9 | 2024-03-31 | Ch 14-15    |        2 | Advanced iteration patterns and functions                                              |
|     10 | 2024-04-07 | Ch 16-17    |        2 | Sorting and nesting data (`list`s, `dict`s )                                               |
|     11 | 2024-04-14 | Ch 18-19    |        2 | Documenting and testing a Python program                                               |
|     12 | 2024-04-21 | Ch 20-21    |        2 | Designing an object-oriented (OO) Python program                                       |
|     13 | 2024-04-28 |             |       20 | FINAL EXAM                                                                             |
|     14 | 2024-05-05 |             |        2 | Installing and using external Python packages (`pandas` and `doctest`)                 |
|     15 | 2024-05-12 |             |        2 | Data-driven program design                                                             |
|     16 | 2024-05-19 |             |       20 | FINAL PROJECT: A data-driven application.                                              |