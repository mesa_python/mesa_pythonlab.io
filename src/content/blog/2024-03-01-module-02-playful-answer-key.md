---
title: '1.6 Playful Python Answers'
description: "In this lesson you will see some really cool answers for the playful python programming assignment."
pubDate: 'March 1, 2024'
---

Here are my answers to the "Module 1 Playful Python" programming assignment.
You can learn some tricks to help you learn python just by fooling around in a Python console.
If this is the way you like to learn, you might try out the iPython console in next week's assignments, after I show you how to install `Anaconda` and `git-bash`.

### Clarification

First I want to clarify the assignment.
You only needed to copy and paste a single line of Python code in order to get full credit.
Whenever I suggest that you "reflect" on something in an assignment, that is a hint about how to *think*. 
That is not something you need to tell me about.
That's the "quiet part" ``;-)``.
You do not explicitly answer the reflection suggestions within the text box for the assignment.

For this assignment you only need to paste Python code into the text box to answer this question correctly.

### Simplest correct answer
Most of you guessed that you can print ``integer``s and ``float``s in Python.
That's probably the easiest answer that will give you full credit.
I've included a comment below the Python code to show you what prints out:

```python
print(42)
# 42

print(123)
# 123

print(3.14)
# 3.14
```

You only needed to paste the text `print(42)` in the text box to get full credit.
The rest of these examples are to help you learn more about Python and prepare you for your own Python programming adventures in Module 2 and 3.
If you play around with these advanced ideas in your own Runestone code blocks you can unlock additional playful python skills.

### Big numbers

Any number is fine for printing.
You can even use exponential notation to print out big numbers like `1,000`:

```python
print(1e3)
# 1000.0
```

Notice that Python doesn't use commas in printed numbers.
This is to ensure that you can copy the printed number and use it in your code.
If you tried to type a number using Python, you would get something unexpected

```python
print(1,234)
# 1 234
```

That is not at all what it looks like.
That is printing out two different numbers, 1 and 234.
Python uses the comma to separate *arguments* to a function, such as the `print()` function.
And a better way to write this code, to be more clear is with a space after commas.
This is a habit you should get into, to make your code easier to read and debug.
Always put a space after a comma, just like you would when writing an English sentence.
Remember, Python expressions are designed to look as much as possible like an English expression.

```python
print(1, 234)
# 1 234
```

You get style points (and cred with you teammates) when you use spaces after commas, but the Python interpretter will do the exact same thing, either way.

If you want to use big numbers inside your code, and make them readable, instead of using a comma as a separator, use an underscore character `_`.
Basically Python just ignores any underscores that appear inside a sequence of digits:

```python
print(1_234)
# 1234

print(1_000_000)
# 1000000
```

You can see how one million is easier to read when you use underscores.
But the printed output is the same, because the number is the same.
Underscores are just style points for your code.

### Containers

Some of you looked ahead in the textbook and learned about Python container types like `list`s and `tuple`s and even `set`s.
These are ways you might have created containers in Python by just playfully hunting and pecking on your keyboard:

```python
print([])
# []

print([])
# ()

print({})
# {}
```

Another way to create these objects is to use built-in `type`s: `list`, `tuple`, `set`.
Don't worry if you didn't think of these.
These would be really hard to discover, just by banging on the keyboard:

```python
print(list())
# []

print(tuple())
# ()

print(set())
# {}
```

Notice that Python doesn't print out the code directly, it prints out the evaluated code.
Each one of these functions returns an object.
The `list()` function returns an empty list, just like the expression `[]`.
So the output looks the same.

### My favorite

Here's my favorite way to ask this question:

```python
print(print)
# <built-in function print>
```

It turns out that everything in Python is an object.
Even `print` is a function!

### Function objects

Can you guess what happens when you run a function inside of a print statement?
Look back at the `print(list())` example and think about what a function does before you look at my answer below.

```python
print(print(42))
# 42
# None
```

Each function is evaluated in sequence, starting at the *inner-most parentheses*.
And the outer `print` function is printing out the object that was returned by the inner print function.
And that object is None.
Rather than returnning the input, Python print functions *always* return a `None` object.
It turns out that `None` is a valid Python expression for creating one of these special `None` objects:

```python
print()
# None

print(None)
# None

print(print())
# None
# None
```

There are a few other special objects similar to `None`.
The objects `True` and `False` are useful when you need to answer a ``True``/``False`` question in your code:

```python
print(True)
# True

print(False)
# False
```

And an empty print function returns the same None object, even when you are printing something out other than None.
It's only the *return value* of the `print` function that is *always* ``None``.

```python
print(print(4))
4
None
```

### Hidden objects

There are many, many many more hidden "Easter Eggs" in Python.
In most other languages, it is really hard to discover and use hidden objects.
But Python made it easy!

```python
print(__name__)
# __main__
```

System variables that are hidden have a "dunder" prefix and suffix.
The word "dunder" is just an easier way of saying double-underscore quickly.
"Underscore" means `_` and "dunder" means `__`.
Dunder main (``__name__``) is a really cool variable that you can use to create Python scripts that know where they are running!

### Advanced answers

Here are some really advanced ways to answer this playful python print question:

```python
print([i for i in range(4))
# [0, 1, 2, 3]
```


```python
print((i for i in range(4)))
# <generator object <genexpr> at 0x7fe3fab76a40>
```

```python
import builtins

print(list(dir(builtins)))
# ['ArithmeticError', ... 'tuple', 'type', 'vars', 'zip']
```

```python
for obj_name in dir(builtins):
    print(obj_name)
# ArithmeticError
# AssertionError
# AttributeError
# BaseException
# ...
# tuple
# type
# vars
# zip
```

