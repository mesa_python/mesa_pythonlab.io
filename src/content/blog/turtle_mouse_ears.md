---
title: 'Red mouse ears'
description: "In chapter 6 of _Foundations of Python Programming_ you will practice your understanding of what a programming language is and what it's used for."
pubDate: 'Feb 26, 2024'
---

# Red mouse ears
The Turtle (line drawing) exercise for Module 4 was tricky! Here's one way to make it work.

### Sidenote for Linux users
On Linux, you will likely need to add the Tkinter (tk) package to python.
It's used to create Graphical User Interfaces (GUI) like the windows that `Turtle` uses to display line drawings in the `Turtle` module.

```bash
$ sudo apt install python3-tk
```

## Using Turtle to draw a mouse
Just as in all the other exercises, you need to create `Screen` and `Turtle` objects before you start drawing.
Call your turle mickey to match the code in Runestone.

```python
>>> import turtle
>>> turtle
<module 'turtle' from '/usr/lib/python3.10/turtle.py'>
```

```python
>>> wn = turtle.Screen()
>>> wn
<turtle._Screen at 0x7fc414e2fc10>
>>> mickey = turtle.Turtle()
>>> mickey
<turtle.Turtle at 0x7fc4163f93f0>
```

I like how Python prints out the type or class name for your objects whenever you print them to the console.

Now you're ready to draw a circle with radius 50.

```python
>>> mickey.circle(50)
```

And now you need to move the pen to the upper left side of the circle.
The pen is pointing to the right (East) when you completed the circle.
So you will need to turn the pen arrow to the left more than 90 degrees.
Amazingling 110 degrees is perfect amount to turn if you plan to move 110 pixels in that direction before drawing a circle.
It would take a lot of trigonometry and geometry to understand why 110 is the right amount.
Fortunately you were given this value as well as all the other numbers you need for this problem.
110 degrees will turn the pen to face 20 degrees to the left of directly up.
So turn the pen (arrow) 110 degrees.

```python
>>> mickey.left(110)
```

Now you're ready to move 110 pixels to the upper left, but don't forget to pick up the pen from the paper so it doesn't leave a line.
And put the pen back down on the paper when you're in position, to get ready to draw the circle.
 
```python
>>> mickey.penup()
... mickey.forward(110)
... mickey.pendown()
```

Now you need to create a circle, but this time you want to fill it in with red.
Can you guess what the Turtle programmers named their function for filling in circles with a color?

If you try to specify the fill color as an argument to the `circle()` method you'll run into trouble.
There's a separate `fillcolor()` method on the `Turtle` object that you named `mickey`.
You can figure out how to specify a color by using the `help()` function built into Python.

```python
help(mickey.fillcolor)
Help on method fillcolor in module turtle:
fillcolor(*args) method of turtle.Turtle instance
    Return or set the fillcolor...
```

If you prefer you can also use the `?` symbol at the end of the function name (before parentheses): `mickey.fillcolor?` 
Your IDE or ipython console may also give you some help when you hit the `[TAB]` key after opening the parenthesis: `mickey.fillcolor(`**`[TAB]`**.
Of course you can always RTFM (read the docs) if these built-in help tools aren't helping you.

Unfortunately, you may still run into trouble when you try to enter a string for your color like 'red':

```python
>>> mickey.fillcolor('red')  # this will raise an Exception!
```

And even if you try to use an RGB tripple of numbers, like you would do in HTML, that would still fail:

```python
>>> mickey.fillcolor((255, 0, 0))  # this will raise an Exception!
```

The reason is that Turtle defaults to a color mode that specifies pixel color intensities with values between 0 and 1 or between 0 and 255 ... depending on what `colormode` has been set for that particular turtle!
Fortunately you can retrieve the colormode value (max pixel value) using the `colormode()` function in the `turtle` module: 

```python
>>> turtle.colormode()
1.0
```

You could just use `1.0` in place of `255` to set the fill color to ``(1.0, 0, 0)``.
But a better way is to store the colormode variable in a variable, so you don't have to remember it, and your code will keep working even if someone changes the color mode to 255 on their machine.
And in iPython it's a good idea to put any new variable you create on a line by itself, to make sure it contains what you expect.
Or you can use the `print` function if you are using a vanilla Python console instead of iPython.

```python
>>> colormode = turtle.colormode()
>>> colormode
1.0
```

```python
>>> mickey.fillcolor((colormode, 0, 0))
... mickey.begin_fill()
... mickey.circle(30)
... mickey.end_fill()
...
>>> mickey.penup()
... mickey.backward(110)
... mickey.pendown()
...
>>> mickey.right(70)
... mickey.forward(110)
...
```

Here's the program from start to finish:

```python
import turtle

wn = turtle.Screen()
mickey = turtle.Turtle()

mickey.circle(50)
mickey.left(110)
mickey.penup()
mickey.forward(110)

colormode = turtle.colormode()
mickey.fillcolor((colormode, 0, 0))

mickey.pendown()
mickey.begin_fill()
mickey.circle(30)
mickey.end_fill()

mickey.penup()
mickey.backward(110)
mickey.right(70)
mickey.forward(110)

mickey.pendown()
mickey.begin_fill()
mickey.circle(30)
mickey.end_fill()
```
