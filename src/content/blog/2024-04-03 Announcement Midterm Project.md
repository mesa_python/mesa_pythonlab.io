---
title: 'Midterm project assignment corrections'
description: 'Corrections to the midterm project (text adventure) instructions and links'
pubDate: 'April 9, 2024'
---

The text adventure assignment turned out to be more of an adventure than I intended.
Many of you successfully submitted working Python programs, though many of you missed some key steps.

1. The correct location for your submission is here: https://gitlab.com/mesa_python/cisc179/-/tree/main/cisc179/adventures
2. You need to sign into your GitLab account before you will see the `[+]` (add file) button on that GitLab page
3. Your GitLab account must be invited to the repository for you to be able to submit additions to the repository, I have invited all of your student e-mail accounts.
4. Your files should be Python files with a `.py` extension
5. You should use a code editor (Spyder) or text editor (notepad) to create your `.py` file
5. You should know how to run your program from the terminal or command line, e.g. by typing `python filename.py`
6. If you use Jupyter Notebook or Jupyter Lab to create and run your program you must use the "Save and Export Notebook as ..." and choose "Executable Script"
7. You should be able to run your Python program within Spyder or from a terminal with the command `python yourlastname.py`
8. If you do not use any `for` or `while` loops, your program should have at least four `if` statements and they should be nested at least 2 layers deep.
9. BONUS: Can you figure out how to make it possible for players in your game to get the same results even when they type their input in all uppercase, all lowercase, or even when they mix capital and lowercase letters?
10. DOUBLE-BONUS: Can you make your program recognize thier input even when they misspell or type only part of the string you are looking for?