---
title: 'Module 2: Runestone chapter 2-3 assignment'
description: "In chapter 2 and 3 of _Foundations of Python Programming_ you will practice your understanding of variables and expressions and try your hand at debugging small Python programs."
pubDate: 'Feb 6, 2024'
---

# Module 2: Runestone chapter 2-3 assignment

Head over to Runestone and try your hand at these questions about the Python variables, expressions, and debugging small Python programs: [Module 2 (Chapter 2 and 3)](https://runestone.academy/assignment/student/doAssignment?assignment_id=173803)
