# ngram_language_model.py
import re


def tokenize_text(text):
    """ Split text into words and punctuation (tokens)

    >>> text = "I'm a Python programmer, and I love it!"
    >>> tokens = tokenize_text(text)
    >>> tokens
    ["I'm", 'a', 'Python', 'programmer', ',', 'and', 'I', 'love', 'it', '!']
    """
    return re.findall(r'[-\'a-zA-Z0-9]+|[!-/:-@\[-`{-~]', text)


def unicode_to_ascii(text):
    """ Convert curly quotes and apostrophes into normal ASCII punctuation characters """
    changes = dict([
        (ord(u), ord(a)) for u, a in zip("‘’´“”–-", "'''\"\"--")
    ])
    return text.translate(changes)
