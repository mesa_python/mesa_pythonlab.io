# Deshittify Windows with Sublime Text and WSL

## Sharing files
You want all the files you edit in Sublime Text to be available outside of WSL. For this to work well, you need to tell Windows file system will "play nice" with WSL.[^1]

#### *`/etc/wsl.conf`*
```toml
[automount]
enabled = true
root = "/mnt/"
options = "uid=1000,gid=1000,umask=22,fmask=11,metadata"
mountFsTab = true
```


[^1]: To work properly Linux needs to be able to set access permissions on files, even if they are maintained on a Windows (NTFS) file system.