import { defineCollection, z } from 'astro:content';

const blog = defineCollection({
	type: 'content',
	// Type-check frontmatter using a schema
	schema: z.object({
		title: z.string(),
		description: z.string(),
		// Transform string to Date object
		pubDate: z.coerce.date(),
		updatedDate: z.coerce.date().optional(),
		heroImage: z.string().optional(),
	}),
});

const data = defineCollection({
    type: 'data',
    // Type-check frontmatter using a schema
    schema: z.object({
        title: z.string().optional(),
        description: z.string().optional(),
// important_dates_md: |+

//   * **01/29**/2024 - Start of Spring Semester
//   * **02/09**/2024 - Drop deadline (drop class with full refund and no “W” on transcript)
//   * **02/16**/2024 - 02/19/2024 - Lincoln/Washington Day (CAMPUS CLOSED)
//   * **03/25**/2024 - 03/29/2024 - (SPRING BREAK)
//   * **03/29**/2024 - Cesar Chavez Day (CAMPUS CLOSED)
//   * **04/12**/2024 - Withdrawal Deadline (Primary 16-Wk Session)
//   * **04/30**/2024 - Deadline to Apply for Graduation
//   * **05/25**/2024 - End of Spring Semester

// prerequisites_md: |+

//   * High School Algebra

// # TODO: generate this from `schedule:` below (see scripts/syllabus_from_yaml.hist.py)
// reading_md: |+

        // // Transform string to Date object
        // pubDate: z.coerce.date().optional(),
        // updatedDate: z.coerce.date().optional(),
        // heroImage: z.string().optional(),
    }),
});

export const collections = { blog, data };
