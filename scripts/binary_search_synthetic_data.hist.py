720*315/560
>>> from datetime import datetime
>>> date_difference = datetime(2024, 1, 1) - datetime(1880, 1, 1)
>>> minutes = list(range(int(date_difference.total_seconds() / 60)))
date_difference
datetime.timedelta(seconds=60)
from datetime import timedelta
timedelta(seconds=60)
dt = datetime(1880, 1, 1)
minute = timedelta(seconds=60)
while dt < datetime(2024,1,1):
    dt += timedelta
dt = datetime(1880, 1, 1)
minute = timedelta(seconds=60)
while dt < datetime(2024,1,1):
    dt = dt + timedelta
dt = datetime(1880, 1, 1)
minute = timedelta(seconds=60)
while dt < datetime(2024,1,1):
    dt = dt + minute
dt = datetime(1880, 1, 1)
minute = timedelta(seconds=60)
timestamps = []
while dt < datetime(2024,1,1):
    timestamps.append(f"{dt.year:04d}-{dt.month:02d}-{dt.day:02d} {dt.hour:02d}:{dt.minute:02d}")
    dt = dt + minute
>>> def find_position(sorted_list, value):
...     """Find the index integer for a value contained in a list."""
...     while len(sorted_list):
...         mid_index = len(sorted_list) // 2
...         mid_value = sorted_list[mid_index]
...         if value == mid_value:
...             return mid_index
...         elif value > mid_value:
...             offset = find_position(sorted_list[mid_index:], value)
...             if offset is None:
...                 return None
...             else:
...                 return offset + mid_index
...         return find_position(sorted_list[:mid_index], value)
timestamps[:10]
timestamps[-10]
find_position(sorted_list, '2023-12-31 23:50')
find_position(timestamps, '2023-12-31 23:50')
%timeit find_position(timestamps, '2023-12-31 23:50')
%timeit find_position(timestamps, '1880-01-01 23:00')
%timeit timestamps.index('1880-01-01 23:00')
%timeit timestamps.index('2023-12-31 23:50')
hist
hist -o -p
>>> dt = datetime(1880, 1, 1)
... minute = timedelta(seconds=60)
... timestamps = []
... while dt < datetime(1900,1,1):
...     timestamps.append(
...         f"{dt.year:04d}-{dt.month:02d}-{dt.day:02d}" 
...         f" {dt.hour:02d}:{dt.minute:02d}"
...         f" {dt.total_seconds / 25_000_000:02.2f}"
...         )
...     dt = dt + minute
start = datetime(1880,1,1)
>>> dt = datetime(1880, 1, 1)
... minute = timedelta(seconds=60)
... timestamps = []
... while dt < datetime(1900,1,1):
...     timestamps.append(
...         f"{dt.year:04d}-{dt.month:02d}-{dt.day:02d}" 
...         f" {dt.hour:02d}:{dt.minute:02d}"
...         f" {(dt - start).total_seconds / 25_000_000:02.2f}"
...         )
...     dt = dt + minute
>>> dt = datetime(1880, 1, 1)
... minute = timedelta(seconds=60)
... timestamps = []
... while dt < datetime(1900,1,1):
...     timestamps.append(
...         f"{dt.year:04d}-{dt.month:02d}-{dt.day:02d}" 
...         f" {dt.hour:02d}:{dt.minute:02d}"
...         f" {(dt - start).total_seconds() / 25_000_000:02.2f}"
...         )
...     dt = dt + minute
timestamps[-10]
>>> dt = datetime(1880, 1, 1)
... minute = timedelta(seconds=60)
... timestamps = []
... while dt < datetime(2024,5,10):
...     timestamps.append(
...         f"{dt.year:04d}-{dt.month:02d}-{dt.day:02d}" 
...         f" {dt.hour:02d}:{dt.minute:02d}"
...         f" {(dt - start).total_seconds() / 4_000_000:02.2f} degC"
...         )
...     dt = dt + minute
hist
timestamps[-2:]
>>> dt = datetime(1880, 1, 1)
... minute = timedelta(seconds=60)
... timestamps = []
... while dt < datetime(2024,5,10):
...     timestamps.append(
...         f"{dt.year:04d}-{dt.month:02d}-{dt.day:02d}" 
...         f" {dt.hour:02d}:{dt.minute:02d}"
...         f" {(dt - start).total_seconds() * 6 / 25_000_000:02.2f} degC"
...         )
...     dt = dt + minute
templogs = timestamps
templogs[-2:]
>>> dt = datetime(1880, 1, 1)
... minute = timedelta(seconds=60)
... timestamps = []
... while dt < datetime(2024,5,10):
...     timestamps.append(
...         f"{dt.year:04d}-{dt.month:02d}-{dt.day:02d}" 
...         f" {dt.hour:02d}:{dt.minute:02d}"
...         f" {(dt - start).total_seconds() * 6 / 1_000_000_000:02.2f} degC"
...         )
...     dt = dt + minute
hist -o -p -f scripts/binary_search_synthetic_data.hist.ipy
hist -f scripts/binary_search_synthetic_data.hist.py
