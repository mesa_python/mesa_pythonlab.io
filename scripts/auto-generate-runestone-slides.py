from pathlib import Path
IMAGES_DIR = Path('.') / 'static' / 'images'
for s in 'instructor student'.split():
    for p in IMAGES_DIR.glob(f'runestone-{s}-0*.png'):
        slide = f'---\n\n## {s.title()} workflow\n\n![{p.name}](/images/{p.name} "Runestone.Academy step {p.name[len('runestone-') + len(c) + 2:-4]}")\n\n'
for s in 'instructor student'.split():
    for p in IMAGES_DIR.glob(f'runestone-{s}-0*.png'):
        slide = f'---\n\n## {s.title()} workflow\n\n'
        slide += f'![{p.name}](/images/{p.name} "Runestone.Academy step {p.name[len("runestone-") + len(c) + 2:-4]}")\n\n'
        print(slide)
for s in 'instructor student'.split():
    for p in IMAGES_DIR.glob(f'runestone-{s}-0*.png'):
        slide = f'---\n\n## {s.title()} workflow\n\n'
        slide += f'![{p.name}](/images/{p.name} "Runestone.Academy step {p.name[len("runestone-") + len(s) + 2:-4]}")\n\n'
        print(slide)
for s in 'instructor student'.split():
    for p in IMAGES_DIR.glob(f'runestone-{s}-0*.png'):
        title = f'Runestone.Academy {s} workflow'
        stepnum = f'p.name[len("runestone-") + len(s) + 2:-4]
        slide = f'---\n\n## {title}\n\n'
        slide += f'#### Step {stepnum}\n'
        slide += f'![{p.name}](/images/{p.name} "{title} - step {stepnum}")\n\n'
        print(slide)
for s in 'instructor student'.split():
    for p in IMAGES_DIR.glob(f'runestone-{s}-0*.png'):
        title = f'Runestone.Academy {s} workflow'
        stepnum = p.name[len("runestone-") + len(s) + 2:-4]
        slide = f'---\n\n## {title}\n\n'
        slide += f'#### Step {stepnum}\n'
        slide += f'![{p.name}](/images/{p.name} "{title} - step {stepnum}")\n\n'
        print(slide)
for s in 'instructor student'.split():
    for p in IMAGES_DIR.glob(f'runestone-{s}-0*.png'):
        title = f'Runestone.Academy {s} workflow'
        stepnum = p.name[len("runestone-") + len(s) + 2:-4]
        slide = f'---\n\n## {title}\n\n'
        slide += f'#### Step {stepnum}\n'
        slide += f'![{p.name}](/images/{p.name} "{title} - step {stepnum}")\n\n'
        print(slide)
ls
pwd
cp static/images/runestone-student-* ../2024/mesa_python.gitlab.io/static/images/
cp static/images/runestone-instructor-* ../2024/mesa_python.gitlab.io/static/images/
hist -f scripts/auto-generate-runestone-slides.py
