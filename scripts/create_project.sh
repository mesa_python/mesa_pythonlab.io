wget https://get.pnpm.io/install.sh
mv install.sh ~/bin/installers/install_pnpm.sh
source ~/bin/installers/install_pnpm.sh
# echo "alias pnpx='pnpm dlx'" >> ~/bin/bash_aliases.sh
# echo "alias pnpmx='pnpm dlx'" >> ~/bin/bash_aliases.sh
source ~/.bashrc
mkdir slides
cd slides
npm create svelte
npm exec svelte-add tailwindcss  # also installs PostCSS
npm install reveal.js @types/reveal.js @fontsource/manrope @fontsource/jetbrains-mono
mkdir scripts
# hist | tail -n 20 >> scripts/create_project.sh
