import plotly.graph_objs as go

# Create a list of node coordinates
Xn = [pos[k][0] for k in pos.keys()]
Yn = [pos[k][1] for k in pos.keys()]

# Create a list of edge coordinates
Xe = []
Ye = []
for e in G.edges():
    Xe.extend([pos[e[0]][0], pos[e[1]][0], None])
    Ye.extend([pos[e[0]][1], pos[e[1]][1], None])

# Create the edge trace
edge_trace = go.Scatter(x=Xe, y=Ye, mode='lines', line=dict(color='gray', width=1))

# Create the node trace
node_trace = go.Scatter(x=Xn, y=Yn, mode='markers+text', text=list(G.nodes()), textposition='bottom center',
                        marker=dict(symbol=node_shapes, size=20, color=node_colors, line=dict(color='black', width=0.5)))

# Create the layout
layout = go.Layout(title='Interactive Network Graph', showlegend=False, hovermode='closest')

# Create the figure
fig = go.Figure(data=[edge_trace, node_trace], layout=layout)

# Show the figure
fig.show()
