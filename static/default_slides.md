# Markdown external file

You can write the entire presentation in Markdown using an external Markdown file.

---


## Markdown code block

```js
function love() {
	console.log('Svelte')
}
```

---

## Markdown code block
#### with line numbers

Line 1, then 2, then all 3

```js [1|2|1-3]
function love() {
	console.log('Svelte')
}
```
