# Runestone

---

## Week 1 (student)

1. **Register and select course name**
2. Chapter 1 reading comprehension exercises 
3. Answer questions within textbook
4. Discuss chapter in Canvas

---

## `Runestone.academy` Home Page

[![Runestone Home Page](/images/runestone-homepage.png)](https://runestone.academy/)

---

## Donation request

![Donation request](/images/runestone-donation-not-today-button.png "Donation request")

---

## Each week (student)

2. **Chapter X reading comprehension exercises**
3. Answer questions within textbook
4. Discuss chapter in Canvas

---

## Each week (student)

2. Chapter X reading comprehension exercises
3. **Answer questions within textbook**
4. Discuss chapter in Canvas

---

## Each week (student)

2. Chapter X reading comprehension exercises
3. Answer questions within textbook
4. **Discuss chapter in Canvas**

---

## Week 1 (teacher)

1. Select book
2. Create course ID (name)
3. Copy assignments from previous semester
4. Select additional assignments for the week

---

## Each week (teacher)

1. Currate assignments
2. Create discussion topic/question
3. Facilitate discussion
3. Autograde assignments from previous week
4. Copy previous weeks' grades to canvas

---

## Markdown external file

You can write the entire presentation in Markdown using an external Markdown file.

---


## Markdown code block

```js
function love() {
	console.log('Svelte')
}
```

---

## Markdown code block
#### with line numbers

Line 1, then 2, then all 3

```js [1|2|1-3]
function love() {
	console.log('Svelte')
}
```